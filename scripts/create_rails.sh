pwd
ls -lrt
echo "gem version: `gem --version`"
echo "Install gems:"
gem list
echo "Gem env:"
gem env
echo "Bundle env:"
bundle env
echo "Create rails-app"
rails new rails-app --database=mysql --skip-coffee 
cd ./rails-app

echo "Install some gems manually"
gem install bindex -v '0.5.0'
#gem install tzinfo-data
echo "Install tzinfo-data gem"
sed -i -e "s/^gem\ \'tzinfo-data\'.*$/gem\ \'tzinfo-data\'/" Gemfile
echo "gem 'unicorn'" >> Gemfile
tail Gemfile
echo "Bundle install"
bundle update
echo "Add AdminLTE with yarn"
yarn add admin-lte@2.4.3
echo "Create sample controller"
bundle exec rails g controller sample starter
echo "Apply template config files"
cp -p ../templates/unicorn.rb ./config/
cp -p ../templates/application.css ./app/assets/stylesheets/
cp -p ../templates/application.js ./app/assets/javascripts/
cp -p ../templates/application.html.erb ./app/views/layouts/
cp -p ../templates/starter.html.erb ./app/views/sample/
cp -p ../templates/user-128x128.png ./app/assets/images/
sed -i -e "s/^\ \ get\ \'sample\/starter\'.*$/\ \ root\ \'sample#starter\'\n\ \ get\ \'sample\/starter\'/" config/routes.rb
sed -i -e "s/^\ \ host:.*$/\ \ host:\ <%=\ ENV[\'DB_HOST\']\ %>/" config/database.yml
sed -i -e "s/^\ \ database:.*$/\ \ database:\ <%=\ ENV[\'MYSQL_DATABASE\']\ %>/" config/database.yml
sed -i -e "s/^\ \ username:.*$/\ \ username:\ <%=\ ENV[\'DB_USER\']\ %>/" config/database.yml
sed -i -e "s/^\ \ password:.*$/\ \ password:\ <%=\ ENV[\'DB_PASSWORD\']\ %>/" config/database.yml
