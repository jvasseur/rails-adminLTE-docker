# Getting started

Run commands from the project root directoy.

`docker-compose -f docker/docker-compose.create-rails.yml build --no-cache`

`docker-compose -f docker/docker-compose.create-rails.yml up --force-recreate`

`docker-compose -f docker/docker-compose.dev.yml build`

`docker-compose -f docker/docker-compose.dev.yml up`

The application can be access at `http://localhost:3000`.
